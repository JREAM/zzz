<?php
namespace Zzz;

/**
 * Timer
 *
 * Measures time for seeing how long operations take
 *
 * @usage
 *     Timer::outputStop(true);
 *     Timer::start('loop');
 *
 *     for ($i = 0; $i < 100; $i++) {
 *         Timer::start('inner_loop');
 *         // do something here
 *         Timer::stop('inner_loop');
 *     }
 *
 *     Timer::stop('loop');
 */
class Counter
{

    /**
     * Saved timers
     * @var array
     */
    public static $_timer = [];

    /**
     * A list of Paused timers
     * @var array
     */
    public static $_timerPaused = [];

}
// End of File
// ------------------------------------------------------------------------
