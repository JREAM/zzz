<?php

/**
 * Registry Pattern Class
 * - Possible one of the most useful design patterns
 *
 * @usage:
 *     \Registry::set('ci', get_instance());
 *     \Registry::set('redis', $this->redis);
 *
 *     $this->ci = \Registry::get('ci');
 *     $this->redis = \Registry::get('redii');
 */
class Registry {

    /**
     * Storage Array
     * @var array
     */
    private static $_storage = [];

    // ------------------------------------------------------------------------

    /**
     * Set an item
     *
     * @param string $key
     *
     * @param void
     */
    public static function set($key, $value)
    {
        self::$_storage[$key] = $value;
    }

    // ------------------------------------------------------------------------

    /**
     * Get an item
     *
     * @param  string $key
     *
     * @return mixed
     */
    public static function get($key)
    {
        return self::$_storage[$key];
    }

    // ------------------------------------------------------------------------

}
// End of File
// ------------------------------------------------------------------------