<?php
namespace Zzz;

/**
 * Timer
 *
 * - Used for Testing Time
 * - Unlimited timers can run at once
 *
 * @usage
 *
 *  No Output
 *  ----------------------
 *  Timer::start('mysql');
 *  Timer::start('loop');
 *  Timer::stop('loop');
 *  Timer::stop('mysql');
 *
 *  echo Timer::get('loop');
 *  echo Timer::get('mysql');
 *
 *  Auto Output
 *  ----------------------
 *  Timer::start('automatic'); // Outputs
 *  Timer::end('automatic');   // Outputs
 *
 */
class Timer
{

    /**
     * Saved timers
     * @var array
     */
    public static $timers = [];

    // ------------------------------------------------------------------------

    /**
     * Start Timer
     *
     * @param  string  $name     Unique name for the timer
     * @param  boolean  $output  (Default: false) Display the output
     *
     * @return void
     */
    public static function start($name, $output = false)
    {
        if (isset(self::$timers[$name])) {
            printf("\nYou already have a timer: %s", $name);
            return;
        }

        // Start our timer!
        self::$timers[$name] = self::_createTime();

        if ($output) {
            printf("\n::Timer[%s] {start}", $name);
        }
    }

    // ------------------------------------------------------------------------

    /**
     * Stop Timer
     *
     * @param  string   $name    Unique name of existing timer
     * @param  boolean  $output  (Default: true) Display the output
     *
     * @return float
     */
    public static function stop($name, $output = true)
    {
        if (!isset(self::$timers[$name])) {
            printf("\nTimer not found: %s", $name);
            return;
        }

        self::$timers[$name] = round(self::_createTime() - self::$timers[$name], 4);

        if ($output) {
            printf("\n::Timer[%s] {stop}  %s", $name, self::$timers[$name]);
        }

        return self::$timers[$name];
    }

    // ------------------------------------------------------------------------

    /**
     * Get a timer
     *
     * @param  string $name
     *
     * @return float
     */
    public static function get($name)
    {
        if (!isset(self::$timers[$name])) {
            printf("\nNo timer exists with: %s", $name);
            exit;
        }

        $time = (float) self::$timers[$name];
        return $time;
    }

    // ------------------------------------------------------------------------

    private static function _createTime()
    {
        $time = explode(' ', microtime());
        return $time[0] + $time[1];
    }

}
// End of File
// ------------------------------------------------------------------------
